$(document).ready(function () {
    let counter = 0;

    let newsList = $(`.news-block-list`);
    let newsRefreshBtn = $(`.news-refresh-btn`);
    let pageNumber = 1;
    let numberAllPages;
    let inputPageNumber = $(`.page-indicator input`);
    let prevBtn = $(`.previous-page-btn`);
    let nextBtn = $(`.next-page-btn`);

    let apiKey = `02e7b5d9-aff0-4a1e-9a08-3c7c25e7703b`;
    let serverUrl = `https://content.guardianapis.com/search?page=${pageNumber}&api-key=${apiKey}`;

    httpGet(serverUrl);


    function count() {
        if (counter < 9) {
            counter++;
            httpGet(serverUrl);
        } else {
            counter = 0;
        }
    }

    function serverUrlRefresh() {
        serverUrl = `https://content.guardianapis.com/search?page=${pageNumber}&api-key=${apiKey}`;
    }

    function httpGet(url) {
        return new Promise(function(resolve, reject) {

            let xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function() {
                if (this.status == 200) {
                    $.get(url, function (data) {
                        newsList.append(`<li class="news-block-el">
                                            <input type="checkbox" value="${data.response.results[counter].apiUrl}" checked>
                                            <h2 class="news-block-el-title">${data.response.results[counter].webTitle}</h2>
                                            <i></i>
                                         </li>`);
                        numberAllPages = data.response.pages;
                        $(`.page-indicator span`).text(numberAllPages);
                    });
                    $(`.news-block p`).css(`display`, `none`);
                    count();
                    resolve(`Done`);
                } else {
                    let error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };

            xhr.onerror = function() {
                reject(new Error("Network Error"));
            };

            xhr.send();
        })
            .catch(error => {
                $(`.error-msg`).css(`display`, `block`);
                newsRefreshBtn.attr(`disabled`, false);
                inputPageNumber.attr(`disabled`, false);
                prevBtn.attr(`disabled`, true);
                nextBtn.attr(`disabled`, true);
            });
    }

    function newsTextGet(url, block) {
        return new Promise(function(resolve, reject) {
            let newsText;
            let newsLink;

            let xhr = new XMLHttpRequest();
            xhr.open('GET', url, true);

            xhr.onload = function() {
                if (this.status == 200) {
                        $.get(url, function (data) {
                            newsText = data.response.content.blocks.body[0].bodyHtml;
                            newsLink = data.response.content.webUrl;
                            newsText = `${newsText.split(`</p>`)[0]}</p>`;
                            block.append(`<div class="news-block-el-content">
                                            ${newsText}
                                            <a class="news-block-el-link" href="${newsLink}" target="_blank">Read full news</a>
                                          </div>`);
                        });
                    resolve(newsText);
                } else {
                    let error = new Error(this.statusText);
                    error.code = this.status;
                    reject(error);
                }
            };

            xhr.onerror = function() {
                reject(new Error("Network Error"));
            };

            xhr.send();
        });
    }
    
    newsList.bind(`DOMSubtreeModified`, function () {
        if(newsList.find(`.news-block-el-title`).length === 10) {
            newsRefreshBtn.attr(`disabled`, false);
            $(`.news-block-el input`).attr(`disabled`, false);
            $(`.news-block-el input`).css(`cursor`, `pointer`);
            inputPageNumber.attr(`disabled`, false);
        } else {
            newsRefreshBtn.attr(`disabled`, true);
            $(`.news-block-el input`).attr(`disabled`, true);
            $(`.news-block-el input`).css(`cursor`, `default`);
            inputPageNumber.attr(`disabled`, true);
        }

        if (pageNumber == 1|| newsList.find(`.news-block-el-title`).length !== 10) {
            prevBtn.attr(`disabled`, true);
        } else {
            prevBtn.attr(`disabled`, false);
        }

        if (pageNumber == numberAllPages || newsList.find(`.news-block-el-title`).length !== 10) {
            nextBtn.attr(`disabled`, true);
        } else {
            nextBtn.attr(`disabled`, false);
        }

        if (inputPageNumber.val() === ``) {
            inputPageNumber.val(pageNumber);
        }
    });

    newsRefreshBtn.click(function () {
        $(`.news-block-list`).empty();
        httpGet(serverUrl);
    });

    newsList.on(`click`,`input`, function () {
        if (this.checked === false  && newsList.find(`.news-block-el-title`).length === 10) {
            if ($(this).siblings(`.news-block-el-content`).length === 0) {
                newsTextGet(`${this.value}?show-blocks=body&api-key=${apiKey}`, $(this).parent());
            }
            $(this).parent().css(`background`, `lightblue`)
        } else {
            $(this).parent().css(`background`, `none`);
        }
    });
    
    nextBtn.click(function () {
        $(`.news-block-list`).empty();
        pageNumber++;
        inputPageNumber.val(pageNumber);
        serverUrlRefresh();
        httpGet(serverUrl);
    });

    prevBtn.click(function () {
        $(`.news-block-list`).empty();
        pageNumber--;
        inputPageNumber.val(pageNumber);
        serverUrlRefresh();
        httpGet(serverUrl);
    });

    inputPageNumber.keydown(function (e) {
        if (e.keyCode === 13) {
            $(`.news-block-list`).empty();
            pageNumber = inputPageNumber.val();
            serverUrlRefresh();
            httpGet(serverUrl);
        }
    });
});